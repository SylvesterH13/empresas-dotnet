﻿namespace InternetMediaDatabase
{
    public static class MensagensResposta
    {
        public static readonly string FilmeNaoEncontrado = "Não foi encontrado filme com o Id informado";
        public static readonly string UsuarioNaoEncontrado = "Não foi encontrado usuário com o Id informado";
        public static readonly string UsuarioNomeJaCadastrado = "Já existe um usuário cadastrado com o nome informado";
        public static readonly string UsuarioAtualizacaoNaoPermitida = "Para atualizar outro usuário é preciso ser administrador";
        public static readonly string UsuarioInativacaoNaoPermitida = "Para inativar outro usuário é preciso ser administrador";
        public static readonly string UsuarioCadastroAdministradorNaoPermitido = "Para cadastrar um usuário administrador é preciso ser administrador";
    }
}