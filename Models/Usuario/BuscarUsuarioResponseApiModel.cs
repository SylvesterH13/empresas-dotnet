﻿namespace InternetMediaDatabase.Models.Usuario
{
    public class BuscarUsuarioResponseApiModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}