﻿namespace InternetMediaDatabase.Models.Usuario
{
    public class BuscarUsuarioRequestApiModel
    {
        public int? Limite { get; set; }
        public int? Deslocamento { get; set; }
    }
}