﻿using System.ComponentModel.DataAnnotations;

namespace InternetMediaDatabase.Models.Usuario
{
    public class UsuarioRequestApiModel
    {
        [Required]
        public string Nome { get; set; }

        [Required]
        public string Senha { get; set; }

        public int TipoId { get; set; }
    }
}