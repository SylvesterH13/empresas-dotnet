﻿using System.ComponentModel.DataAnnotations;

namespace InternetMediaDatabase.Models.Voto
{
    public class VotoRequestApiModel
    {
        public int FilmeId { get; set; }

        [Range(0,4)]
        public int Nota { get; set; }
    }
}