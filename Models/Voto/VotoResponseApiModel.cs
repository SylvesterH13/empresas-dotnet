﻿namespace InternetMediaDatabase.Models.Voto
{
    public class VotoResponseApiModel
    {
        public string Usuario { get; set; }
        public int Nota { get; set; }
    }
}