﻿using InternetMediaDatabase.Models.Voto;
using System.Collections.Generic;

namespace InternetMediaDatabase.Models.Filmes
{
    public class BuscarFilmeResponseApiModel : FilmeBaseApiModel
    {
        public int Id { get; set; }
        public IEnumerable<VotoResponseApiModel> Votos { get; set; }
    }
}