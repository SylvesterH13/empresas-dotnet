﻿namespace InternetMediaDatabase.Models.Filmes
{
    public abstract class FilmeBaseApiModel
    {
        public string Nome { get; set; }
        public string Genero { get; set; }
        public string Diretor { get; set; }
    }
}