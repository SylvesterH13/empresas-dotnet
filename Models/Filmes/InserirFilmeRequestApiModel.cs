﻿using System.ComponentModel.DataAnnotations;

namespace InternetMediaDatabase.Models.Filmes
{
    public class InserirFilmeRequestApiModel
    {
        [Required]
        public string Nome { get; set; }

        [Required]
        public string Genero { get; set; }

        [Required]
        public string Diretor { get; set; }
    }
}