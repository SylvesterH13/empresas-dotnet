﻿namespace InternetMediaDatabase.Models.Filmes
{
    public class BuscarFilmesRequestApiModel : FilmeBaseApiModel
    {
        public int? Descolamento { get; set; }
        public int? Limite { get; set; }
    }
}