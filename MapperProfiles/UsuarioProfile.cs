﻿using AutoMapper;
using InternetMediaDatabase.Entidades;
using InternetMediaDatabase.Models.Usuario;

namespace InternetMediaDatabase.MapperProfiles
{
    public class UsuarioProfile : Profile
    {
        public UsuarioProfile()
        {
            CreateMap<Usuario, BuscarUsuarioResponseApiModel>();
            CreateMap<UsuarioRequestApiModel, Usuario>()
                .ForMember(dest => dest.Ativo, opt => opt.MapFrom(src => true));
        }
    }
}