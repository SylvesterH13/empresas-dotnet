﻿using AutoMapper;
using InternetMediaDatabase.Entidades;
using InternetMediaDatabase.Models.Filmes;

namespace InternetMediaDatabase.MapperProfiles
{
    public class FilmeProfile : Profile
    {
        public FilmeProfile()
        {
            CreateMap<InserirFilmeRequestApiModel, Filme>();
            CreateMap<Filme, BuscarFilmeResponseApiModel>();
        }
    }
}