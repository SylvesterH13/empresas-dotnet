﻿using AutoMapper;
using InternetMediaDatabase.Entidades;
using InternetMediaDatabase.Models.Voto;

namespace InternetMediaDatabase.MapperProfiles
{
    public class VotoProfile : Profile
    {
        public VotoProfile()
        {
            CreateMap<Voto, VotoResponseApiModel>()
                .ForMember(dest => dest.Usuario, opt => opt.MapFrom(src => src.Usuario.Nome));
        }
    }
}