﻿using InternetMediaDatabase.Entidades;
using InternetMediaDatabase.Models.Token;
using InternetMediaDatabase.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace InternetMediaDatabase.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly InternetMediaDBContext _context;
        private readonly TokenService _tokenService;

        public TokenController(InternetMediaDBContext context, TokenService tokenService)
        {
            _context = context;
            _tokenService = tokenService;
        }

        [HttpGet]
        public ActionResult GetToken([FromQuery] TokenRequestApiModel model)
        {
            var usuario = _context.Usuarios.SingleOrDefault(u => u.Nome == model.Usuario);
            if (usuario is null)
                return BadRequest("Usuário não encontrado");

            if (usuario.Senha != model.Senha)
                return BadRequest("Senha inválida");

            var token = _tokenService.GerarToken(usuario);

            return Ok(token);
        }
    }
}