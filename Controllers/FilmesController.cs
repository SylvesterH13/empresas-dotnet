﻿using AutoMapper;
using InternetMediaDatabase.Entidades;
using InternetMediaDatabase.Models.Filmes;
using InternetMediaDatabase.Models.Voto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetMediaDatabase.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FilmesController : ControllerBase
    {
        private readonly InternetMediaDBContext _context;
        private readonly IMapper _mapper;

        public FilmesController(InternetMediaDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        public async Task<IActionResult> InserirAsync([FromBody] InserirFilmeRequestApiModel model)
        {
            var filme = _mapper.Map<Filme>(model);

            _context.Filmes.Add(filme);
            await _context.SaveChangesAsync();

            return Created(nameof(BuscarPorIdAsync), filme.Id);
        }

        [HttpGet]
        public async Task<IActionResult> BuscarAsync([FromQuery] BuscarFilmesRequestApiModel requestModel)
        {
            var deslocamento = requestModel?.Descolamento ?? 0;
            var limite = requestModel?.Limite != null && requestModel.Limite != 0 ? requestModel.Limite.Value : int.MaxValue;

            var filmes = await _context.Filmes
                .Where(f => requestModel == null ||
                    ((requestModel.Nome == null || f.Nome == requestModel.Nome) &&
                    (requestModel.Diretor == null || f.Diretor == requestModel.Diretor) &&
                    (requestModel.Genero == null || f.Genero == requestModel.Genero)))
                .Include(f => f.Votos)
                .ToListAsync();

            filmes = filmes.OrderByDescending(f => f.Votos.Count())
                .ThenBy(f => f.Nome)
                .Skip(deslocamento)
                .Take(limite)
                .ToList();

            var responseModel = _mapper.Map<IEnumerable<BuscarFilmeResponseApiModel>>(filmes);

            return Ok(responseModel);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> BuscarPorIdAsync(int id)
        {
            var filme = await _context.Filmes.FindAsync(id);
            if (filme is null)
                return NotFound(MensagensResposta.FilmeNaoEncontrado);

            var model = _mapper.Map<BuscarFilmeResponseApiModel>(filme);

            return Ok(model);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> ExcluirFilme(int id)
        {
            var filme = await _context.Filmes.FindAsync(id);
            if (filme is null)
                return NotFound(MensagensResposta.FilmeNaoEncontrado);

            _context.Filmes.Remove(filme);
            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPost("votar")]
        public async Task<IActionResult> VotarAsync(VotoRequestApiModel model)
        {
            var filme = await _context.Filmes.SingleOrDefaultAsync(f => f.Id == model.FilmeId);
            if (filme is null)
                return BadRequest(MensagensResposta.FilmeNaoEncontrado);

            var usuario = await _context.Usuarios.SingleAsync(u => u.Nome == User.Identity.Name);
            var voto = await _context.Votos.SingleOrDefaultAsync(v => v.Filme.Id == model.FilmeId && v.Usuario.Id == usuario.Id);
            if (voto is null)
            {
                voto = new Voto
                {
                    Nota = model.Nota,
                    Filme = filme,
                    Usuario = usuario
                };
                await _context.Votos.AddAsync(voto);
            }
            else
            {
                voto.Nota = voto.Nota;
                _context.Entry(voto).State = EntityState.Modified;
            }
            
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}