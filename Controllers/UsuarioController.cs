﻿using AutoMapper;
using InternetMediaDatabase.Entidades;
using InternetMediaDatabase.Models.Usuario;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetMediaDatabase.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly InternetMediaDBContext _context;
        private readonly IMapper _mapper;

        public UsuarioController(InternetMediaDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> InserirAsync([FromBody] UsuarioRequestApiModel model)
        {
            if (User.Identity.Name is null && model.TipoId == (int)TipoUsuario.Administrador)
                return BadRequest(MensagensResposta.UsuarioCadastroAdministradorNaoPermitido);

            var usuario = await _context.Usuarios.SingleOrDefaultAsync(s => s.Nome == model.Nome);
            if (usuario != null)
                return BadRequest(MensagensResposta.UsuarioNomeJaCadastrado);

            usuario = _mapper.Map<Usuario>(model);

            _context.Add(usuario);
            await _context.SaveChangesAsync();

            return Ok(usuario.Id);
        }

        [HttpGet]
        [Authorize(Roles = "Administrador")]
        public async Task<IActionResult> BuscarAsync([FromQuery] BuscarUsuarioRequestApiModel requestModel)
        {
            var deslocamento = requestModel?.Deslocamento ?? 0;
            var limite = requestModel?.Limite != null && requestModel.Limite != 0 ? requestModel.Limite.Value : int.MaxValue;

            var usuarios = await _context.Usuarios
                .Where(u => u.Ativo && u.Tipo != TipoUsuario.Administrador)
                .Skip(deslocamento)
                .Take(limite)
                .OrderBy(u => u.Nome)
                .ToListAsync();

            var responseModel = _mapper.Map<List<BuscarUsuarioResponseApiModel>>(usuarios);

            return Ok(responseModel);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> AtualizarAsync(int id, [FromBody] UsuarioRequestApiModel model)
        {
            var usuario = await _context.Usuarios.FindAsync(id);
            if (usuario is null)
                return NotFound(MensagensResposta.UsuarioNaoEncontrado);

            var usuarioLogado = await _context.Usuarios.SingleAsync(u => u.Nome == User.Identity.Name);
            if (usuarioLogado.Tipo != TipoUsuario.Administrador && usuarioLogado.Id != id)
                return BadRequest(MensagensResposta.UsuarioAtualizacaoNaoPermitida);

            if (model.Nome != usuario.Nome && _context.Usuarios.Any(u => u.Nome == model.Nome))
                return BadRequest(MensagensResposta.UsuarioNomeJaCadastrado);

            _mapper.Map(model, usuario);
            _context.Entry(usuario).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPut("{id}/inativar")]
        public async Task<IActionResult> InativarAsync(int id)
        {
            var usuario = await _context.Usuarios.FindAsync(id);
            if (usuario is null)
                return NotFound(MensagensResposta.UsuarioNaoEncontrado);

            var usuarioLogado = await _context.Usuarios.SingleAsync(u => u.Nome == User.Identity.Name);
            if (usuarioLogado.Tipo != TipoUsuario.Administrador && usuarioLogado.Id != id)
                return BadRequest(MensagensResposta.UsuarioInativacaoNaoPermitida);

            usuario.Ativo = false;
            _context.Entry(usuario).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}