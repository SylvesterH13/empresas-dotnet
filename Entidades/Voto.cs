﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace InternetMediaDatabase.Entidades
{
    public class Voto
    {
        public int Id { get; set; }
        public int Nota { get; set; }

        [JsonIgnore]
        [Required]
        public virtual Filme Filme { get; set; }

        [JsonIgnore]
        [Required]
        public virtual Usuario Usuario { get; set; }
    }
}