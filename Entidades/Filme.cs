﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InternetMediaDatabase.Entidades
{
    public class Filme
    {
        public int Id { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public string Genero { get; set; }

        [Required]
        public string Diretor { get; set; }

        public virtual IEnumerable<Voto> Votos { get; set; }
    }
}