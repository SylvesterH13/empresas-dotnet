﻿using System.Collections.Generic;

namespace InternetMediaDatabase.Entidades
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
        public bool Ativo { get; set; }
        public virtual TipoUsuario Tipo { get; set; }
        public virtual IEnumerable<Voto> Votos { get; set; }
    }
}