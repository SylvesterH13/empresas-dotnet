﻿using Microsoft.EntityFrameworkCore;

namespace InternetMediaDatabase.Entidades
{
    public class InternetMediaDBContext : DbContext
    {
        public DbSet<Filme> Filmes { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Voto> Votos { get; set; }

        public InternetMediaDBContext(DbContextOptions<InternetMediaDBContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>().HasData(new Usuario
            {
                Id = 1,
                Nome = "Administrador Teste",
                Senha = "senhaPadrao123!@#",
                Ativo = true,
                Tipo = TipoUsuario.Administrador
            });
        }
    }
}